package com.twuc.webApp.domain.mazeGenerator;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

/**
 * <p>
 * 你看，这两个人叫做 D. Aldous 和 A. Broder。他发明了一种迷宫的算法。可以生成难度均衡的迷宫。它很简单。但是效率欠佳。
 * 如果你想了解更多可以参考
 * </p>
 *
 * <p>http://weblog.jamisbuck.org/2011/1/17/maze-generation-aldous-broder-algorithm</p>
 *
 * <p>但是这并不是重点。不需要在这里花费太多时间。记住，你要了解的是程序的结构。</p>
 */
@Component
public class AldousBroderMazeAlgorithm implements MazeUpdater {

    @Override
    public void update(Grid grid) {
        Random rand = new Random();
        GridCell cell = grid.getRandomCell();
        int unvisited = grid.size() - 1;

        while (unvisited > 0)
        {
            List<GridCell> neighbors = cell.getNeighbors();
            GridCell neighbor = neighbors.get(rand.nextInt(neighbors.size()));
            if (neighbor.getLinks().isEmpty())
            {
                cell.link(neighbor);
                --unvisited;
            }

            cell = neighbor;
        }
    }
}
